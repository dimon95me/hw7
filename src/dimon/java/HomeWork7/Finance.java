package dimon.java.HomeWork7;

import model.MoneyHouse;
import options.Credit;
import options.Exchange;
import options.Investing;
import options.Send;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dimon on 16.04.18.
 */
public class Finance {

    private MoneyHouse[] moneyHouses;

    public Finance(MoneyHouse[] moneyHouses) {
        this.moneyHouses = moneyHouses;
    }

    public void exchangeMinimal(int count, String nameOfVault) {
        Exchange maxExchanger = null;
        for (MoneyHouse moneyHouse : moneyHouses) {
            if (moneyHouse instanceof Exchange) {
                maxExchanger = (Exchange) moneyHouse;
            }
        }

        for (MoneyHouse moneyHouse : moneyHouses) {
            if (moneyHouse instanceof Exchange) {
                if (((Exchange) moneyHouse).exchange(count, nameOfVault) > maxExchanger.exchange(count, nameOfVault)) {
                    maxExchanger = (Exchange) moneyHouse;
                }
            }
        }

        if (maxExchanger.exchange(count, nameOfVault) != 0) {
            ((MoneyHouse) maxExchanger).print();
            System.out.println(maxExchanger.exchange(count, nameOfVault));
        } else {
            System.out.println("Too much count or don't have vault");
        }
    }

    public void creditMinimal(int count) {
        Credit minCreditor = null;
        for (MoneyHouse moneyHouse : moneyHouses) {
            if (moneyHouse instanceof Credit) {
                minCreditor = (Credit) moneyHouse;
                break;
            }
        }

        for (MoneyHouse moneyHouse : moneyHouses) {
            if (moneyHouse instanceof Credit) {
                if (minCreditor.creditRefund(count) == -1) {
                    minCreditor = (Credit) moneyHouse;
                    continue;
                } else if ((((Credit) moneyHouse).creditRefund(count) < minCreditor.creditRefund(count))) {
                    minCreditor = ((Credit) moneyHouse);
                }
            }
        }

        ((MoneyHouse) minCreditor).print();
        System.out.println(minCreditor.creditRefund(count));
    }

    public void invest(int count, int monthCount) {
        Investing investingBest = null;

        for (MoneyHouse moneyHouse : moneyHouses) {
            if (moneyHouse instanceof Investing) {
                investingBest = ((Investing) moneyHouse);
                break;
            }
        }

        for (MoneyHouse moneyHouse : moneyHouses) {
            if (moneyHouse instanceof Investing) {
                if (((Investing) moneyHouse).invest(count, monthCount) > 0 && ((Investing) moneyHouse).invest(count, monthCount) > investingBest.invest(count, monthCount)) {
                    investingBest = ((Investing) moneyHouse);
                }
            }
        }
        ((MoneyHouse) investingBest).print();
        System.out.println(investingBest.invest(count, monthCount));
    }

    public void send(int count) {
        Send sendBest = null;
        //List<Send> sendList = new ArrayList<>();
        for (MoneyHouse moneyHouse : moneyHouses) {
            if (moneyHouse instanceof Send) {
                sendBest = (Send) moneyHouse;
                break;
            }
        }

        for (MoneyHouse moneyHouse : moneyHouses) {
            if (moneyHouse instanceof Send) {
                if (((Send) moneyHouse).send(count) > sendBest.send(count)) {
                    sendBest = ((Send) moneyHouse);
                }
            }
        }
        ((MoneyHouse) sendBest).print();
        System.out.println(sendBest.send(count));
    }

}
