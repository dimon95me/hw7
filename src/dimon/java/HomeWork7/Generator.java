package dimon.java.HomeWork7;

import model.*;

/**
 * Created by Dimon on 16.04.18.
 */
public final class Generator {
    private Generator() {
    }

    public static MoneyHouse[] generate(){
        MoneyHouse[] moneyHouses = new MoneyHouse[7];

        Vault[] exchangerVaults = new Vault[3];
        exchangerVaults[0] = new Vault("UAH", 1f);
        exchangerVaults[1] = new Vault("USD", 26.8f);
        exchangerVaults[2] = new Vault("EUR", 30.5f);
        moneyHouses[0] = new Exchanger("Exchanger supper", "Vatutina street", exchangerVaults);

        moneyHouses[1] = new Creditor("PawnShop", "prospect Nauki", 50000, 40);
        moneyHouses[2] = new Creditor("Credit Cafe", "prospect  Lenina", 4000, 200);
        moneyHouses[3] = new Creditor("CreditUnion", "Ludviga Svobodu street", 100000, 20);

        moneyHouses[4] = new Pif("Paevoy investment fund", "Chicago", 10);

        moneyHouses[5] = new Post("Ukrposhta", "Lasarevskogo street", 2);

        Vault[] bankVaults = new Vault[4];
        bankVaults[0] = new Vault("UAH", 1f);
        bankVaults[1] = new Vault("USD", 27.5f);
        bankVaults[2] = new Vault("EUR", 30.4f);
        bankVaults[3] = new Vault("RUR", 0.45f);
        moneyHouses[6] = new Bank("PrivatBank", "Moskovsky prospekt",bankVaults, 200000, 25, 12);
        return moneyHouses;
    }
}
