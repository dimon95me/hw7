package options;

/**
 * Created by Dimon on 20.04.18.
 */
public interface Investing {
    public int invest(int count, int monthCount);
}
