package options;

/**
 * Created by Dimon on 18.04.18.
 */
public interface Credit {
    public int creditRefund(int count);
}
