package model;

import options.Investing;

/**
 * Created by Dimon on 20.04.18.
 */
public class Pif extends MoneyHouse implements Investing {

    private int percent;
    private final int MIN_MONTH_COUNT = 12;

    public Pif(String name, String address, int percent) {
        super(name, address);
        this.percent = percent;
    }

    @Override
    public int invest(int count, int monthCount) {
        int result = 0;
        if (monthCount >= MIN_MONTH_COUNT) {
            result = (int) ((((float)(percent +100)) / 100) * count); // Можно ли это офоримть как-то попроще?
        }
        return result;
    }

    public void print() {
        System.out.println("---------");
        System.out.println(name);
        System.out.println(address);
        System.out.println(percent);
        System.out.println("MIN_MONTH_COUNT: " + MIN_MONTH_COUNT);
    }
}
