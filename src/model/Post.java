package model;

import options.Send;

/**
 * Created by Dimon on 23.04.18.
 */
public class Post extends MoneyHouse implements Send {

    private int commission;

    public Post(String name, String address, int commission) {
        super(name, address);
        this.commission = commission;
    }


    @Override
    public float send(int count) {
        return ((float) (1-commission*0.01f)*count);
    }

    public void print(){
        System.out.println("-----------");
        System.out.println(name);
        System.out.println(address);
        System.out.println(commission);
    }
}
