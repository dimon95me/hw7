package model;

/**
 * Created by Dimon on 16.04.18.
 */
public class MoneyHouse {
    String name;
    String address;

    public MoneyHouse(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public void print(){
        System.out.println("-----------");
        System.out.println(name);
        System.out.println(address);
    }
}
