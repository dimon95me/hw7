package model;

/**
 * Created by Dimon on 16.04.18.
 */
public class Vault {
    private String name;
    private float cource;

    public Vault(String name, float cource) {
        this.name = name;
        this.cource = cource;
    }

    public String getName() {
        return name;
    }

    public float getCource() {
        return cource;
    }

    public boolean isName(String name){
        return name.equalsIgnoreCase(this.name);

    }

    public float courceWhereName(String name){
        if (name.equalsIgnoreCase(this.name)){
            return cource;
        }
        return 0;
    }
}
