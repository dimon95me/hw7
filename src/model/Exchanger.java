package model;

import options.Exchange;

/**
 * Created by Dimon on 16.04.18.
 */
public class Exchanger extends MoneyHouse implements Exchange {
    private Vault[] vaults;

    public Exchanger(String name, String address, Vault[] vaults) {
        super(name, address);
        this.vaults = vaults;
    }

    @Override
    public float exchange(int count, String nameVault) {
        Vault vault = null;
        for (Vault vault1 : vaults) {
            if (vault1.getName().equalsIgnoreCase(nameVault)) {
                vault = vault1;
            }
        }

        if (vault != null) {
            return count * vault.getCource();
        }
        return 0;
    }

//    @Override
//    public float exchange(int count, String nameVault) {
//        Vault vault = new Vault("noonE", 3f);
//        float result = 0;
//        boolean verify = false;
//        for (Vault vaulta : vaults) {
//            if (vaulta.isName(nameVault)) {
//                vault = new Vault(vaulta.getName(), vaulta.getCource());
//                verify = true;
//            }
//        }
//        if (!verify) {
//            System.out.println("Vault does not found");
//        }
//        return result;
//    }
}
