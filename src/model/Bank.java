package model;

import options.Credit;
import options.Exchange;
import options.Investing;
import options.Send;

/**
 * Created by Dimon on 16.04.18.
 */
public class Bank extends MoneyHouse implements Exchange, Credit, Investing, Send {
    private Vault[] vaults;
    private final int MAX_EXCHANGE_COUNT = 12000;
    private int maxCreditValue;
    private int creditPercent;
    private int investPercent;
    private final int INVEST_MAX_MONTH_COUNT = 12;

    public Bank(String name, String address, Vault[] vaults, int maxCreditValue, int creditPercent, int investPercent) {
        super(name, address);
        this.vaults = vaults;
        this.maxCreditValue = maxCreditValue;
        this.creditPercent = creditPercent;
        this.investPercent = investPercent;
    }

//    @Override
//    public float exchange(int count, String action, String nameVault) {
//        float result = 6;
//        if (count <= MAX_EXCHANGE_COUNT) {
//            Vault vault = new Vault("noonE", 5f);
//            boolean verify = false;
//            for (Vault vaulta : vaults) {
//                if (vaulta.isName(nameVault)) {
//                    vault = new Vault(vaulta.getName(), vaulta.getCource());
//                    verify = true;
//                }
//            }
//            if (!verify) {
//                System.out.println("Vault does not found");
//                return 0;
//            }
//            if (action.equalsIgnoreCase("buy")) {
//                result = count / vault.getCource() - 14;
//            } else if (action.equalsIgnoreCase("sell")) {
//                result = count * vault.getCource() - 15;
//            } else {
//                System.out.println("Wrong action");
//            }
//
//        }
//        return result;
//    }

    @Override
    public int creditRefund(int count) {
        int result = -1;
        if (count <= maxCreditValue) {
            result = (creditPercent * count) / 100 + count;
        }
        return result;
    }

    public void print() {
        System.out.println("---------");
        System.out.println(name);
        System.out.println(address);
        System.out.println(MAX_EXCHANGE_COUNT);
        System.out.println(maxCreditValue);
        System.out.println(creditPercent);
        System.out.println(investPercent);
        System.out.println(INVEST_MAX_MONTH_COUNT);
    }

    @Override
    public int invest(int count, int monthCount) {
        int result = 0;
        if (monthCount <= INVEST_MAX_MONTH_COUNT) {
            result = (int) ((((float) (investPercent + 100)) / 100) * count);
        }
        return result;
        //return (monthCount<=INVEST_MAX_MONTH_COUNT) ? (((investPercent+100)/100)*count) : 0;
    }

    @Override
    public float send(int count) {
        return ((float) (1 - 0.01f) * count) - 5;
    }

    @Override
    public float exchange(int count, String nameVault) {
        Vault vault = null;
        for (Vault vault1 : vaults) {
            if (vault1.getName().equalsIgnoreCase(nameVault)) {
                vault = vault1;
                break;
            }
        }
        float result = 0;
        if (count <= MAX_EXCHANGE_COUNT) {
            result = count * vault.getCource() - 15;
        }
        return result;
    }
}
