package model;

import options.Credit;

/**
 * Created by Dimon on 18.04.18.
 */
public class Creditor extends MoneyHouse implements Credit {
    private int maxValue;
    private int yearPercent;

    public Creditor(String name, String address, int maxValue, int yearPercent) {
        super(name, address);
        this.maxValue = maxValue;
        this.yearPercent = yearPercent;
    }

    @Override
    public int creditRefund(int count) {
        int result = -1;
        if(count<=maxValue){
            result = (yearPercent*count)/100+count;
        }
        return result;
    }

    public void print(){
        System.out.println("-----------");
        System.out.println(name);
        System.out.println(address);
        System.out.println(maxValue);
        System.out.println(yearPercent);
    }
}
